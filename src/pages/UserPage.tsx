import * as React from 'react'
import { RouteComponentProps, Route, Switch, BrowserRouter } from 'react-router-dom';
import { apiGetQuery, apiPostQuery } from '../data/api/APIGQLQuery';
import Center from 'react-center'
import HollowButton from '../components/HollowButton'
import BlotList from '../components/lists/UserBlotList'
import PhotosList from '../components/PhotosList'
import PlacesList from '../components/PlacesList'
import { Loader, Icon, Button, Statistic, Header, Container, Image, Label } from 'semantic-ui-react'
import { message } from 'antd'
import { PRODUCTION } from '../data/debug'

import '../styles/pages/UserPage.css'

interface UserPageParams {

    username: string

}

interface UserPageState {

    loading: boolean,
    userInfo?: UserData,
    selectedTab: string,
    followUserLoading: boolean,
    loggedInUsername?: string
}

interface UserData {
    username?: string,
    displayName?: string,
    followingCount?: string,
    followerCount?: string,
    followsMe?: boolean,
    followedByMe?: boolean
}

export default class UserPage extends React.Component<RouteComponentProps<UserPageParams>, UserPageState> {

    state = {
        loading: false,
        userInfo: null,
        selectedTab: "blots",
        followUserLoading: false,
        loggedInUsername: null
    }

    fetchUserData(user: string = this.props.match.params.username) {
        this.setState({
            loading: true
        })

        apiGetQuery(
            `
                {
                    user(id: "${user}") {
                        username,
                        displayName,
                        followingCount,
                        followerCount,
                        followsMe,
                        followedByMe
                    },

                    me {
                        username
                    }
                }
            `
        ).then((data) => {
            console.log(data)
            this.setState({
                loading: false,
                userInfo: data.data.user,
                loggedInUsername: data.data.me ? data.data.me.username : null
            })
        })
    }

    componentWillMount() {
        this.fetchUserData()
    }

    
    componentWillReceiveProps(newProps: RouteComponentProps<UserPageParams>) {
        console.log("componentWillReceiveProps")
        console.log(this.state.userInfo)
        console.log(newProps.match.params.username)
        if (this.state.userInfo && newProps.match.params.username !== this.state.userInfo.username) {
            console.log("fetch new data")
            this.fetchUserData(newProps.match.params.username)
        }
    }

    render() {
        if (this.state.loading) {

            return (
                <div style={{ width: "100%", height: "100%"}}>
                    <Loader></Loader>
                </div>
            )

        } else {
            return (
                <div className="userPage-page">
                    <div className="userPage-userInfo">
                        <Image centered bordered circular size="small" src={`${PRODUCTION ? "https://blot-backend.herokuapp.com" : "http://localhost:2299"}/content/prof_pic/preview/${this.props.match.params.username}`} />
                        {this.renderUserInfo()}
                    </div>

                    {this.renderContent()}
                </div>
            )
        }
    }

    renderUserInfo() {
        if (this.state.userInfo) {
            return (
                <Container textAlign="center">
                    <Header as="h1">
                        {this.state.userInfo ? this.state.userInfo.displayName : ""}
                        <Header.Subheader>
                            <Label color="black">@{this.state.userInfo.username}</Label>
                        </Header.Subheader>
                    </Header>

                    <Center>
                        <Statistic.Group>
                            <Statistic>
                                <Statistic.Value>{this.state.userInfo.followerCount}</Statistic.Value>
                                <Statistic.Label>Followers</Statistic.Label>
                            </Statistic>
                            <Statistic>
                                <Statistic.Value>{this.state.userInfo.followingCount}</Statistic.Value>
                                <Statistic.Label>Following</Statistic.Label>
                            </Statistic>
                        </Statistic.Group>
                    </Center>

                    {this.renderFollowButton()}
                </Container>
            )
        }
    }

    renderContent() {
        if (this.state.userInfo) {
            return (
                <div className="userPage-content">
                    <Center>
                        <div className="userPage-tabs">
                            <a className={this.getTabHeaderClassName("blots")} onClick={() => this.onTabHeaderClicked("blots")}>blots</a>
                            <a className={this.getTabHeaderClassName("photos")} onClick={() => this.onTabHeaderClicked("photos")}>photos</a>
                            <a className={this.getTabHeaderClassName("places")} onClick={() => this.onTabHeaderClicked("places")}>places</a>
                        </div>
                    </Center>

                    <Container>
                        <Route path={`/u/${this.state.userInfo.username}/`} exact
                            render={() => {
                                return (
                                    <BlotList username={this.state.userInfo.username} />
                                )
                            }}
                        />

                        <Route path={`/u/${this.state.userInfo.username}/blots`}
                            render={() => {
                                return (
                                    <BlotList username={this.state.userInfo.username} />
                                )
                            }}
                        />

                        <Route path={`/u/${this.state.userInfo.username}/photos`}
                            render={() => {
                                return (
                                    <PhotosList />
                                )
                            }} />

                        <Route path={`/u/${this.state.userInfo.username}/places`}
                            render={() => {
                                return (
                                    <PlacesList />
                                )
                            }}
                        />
                    </Container>
                </div>
            )
        } else {
            return (<div />)
        }
    }

    renderFollowButton() {
        if ((this.state.loggedInUsername && this.state.loggedInUsername == this.props.match.params.username) || this.state.loggedInUsername == null) {
            return (
                <div>
                </div>
            )
        }

        if (this.state.userInfo && !this.state.userInfo.followedByMe) {
            return (
                <Button loading={this.state.followUserLoading} onClick={() => this.followUser()} color="black" size="big" icon labelPosition="left"><Icon name="add" /> follow</Button>
            )
        } else {
            return (
                <Button className="followButton" animated="vertical" loading={this.state.followUserLoading} onClick={() => this.unfollowUser()} color="green" size="big">
                    <Button.Content visible>
                        following
                    </Button.Content>
                    <Button.Content hidden>
                        unfollow ?
                    </Button.Content> 
                </Button>
            )
        }
    }

    getTabHeaderClassName(tabID: string): string {
        if (tabID == this.state.selectedTab) return "userPage-section userPage-section-current"
        else return "userPage-section"
    }

    onTabHeaderClicked(tabID: string) {
        if (tabID != this.state.selectedTab) {
            this.props.history.push(`/u/${this.props.match.params.username}/${tabID}`)
            this.setState({
                selectedTab: tabID
            })
        }
    }
    unfollowUser() {
        apiPostQuery(
            `mutation { unfollowUser(id: \\"${this.props.match.params.username}\\") {followerCount,followingCount,followsMe,followedByMe}}`
        ).then((data) => {
            
            this.setState({
                followUserLoading: false,
                userInfo: {
                    username: this.state.userInfo.username,
                    displayName: this.state.userInfo.displayName,
                    followerCount: data.data.unfollowUser.followerCount,
                    followingCount: data.data.unfollowUser.followingCount,
                    followsMe: data.data.unfollowUser.followsMe,
                    followedByMe: data.data.unfollowUser.followedByMe
                }
            })

        }).catch((reason) => {

            this.setState({
                followUserLoading: false
            })

            message.error(`Couldn't unfollow user. Check your Network Connection, Reason : ${reason}`)

        })
    }

    followUser() {
        this.setState({
            followUserLoading: true
        })

        apiPostQuery(
            `mutation { followUser(id: \\"${this.props.match.params.username}\\") { followerCount,followingCount, followsMe, followedByMe }}`
        ).then((data) => {

            this.setState({
                followUserLoading: false,
                userInfo: {
                    username: this.state.userInfo.username,
                    displayName: this.state.userInfo.displayName,
                    followerCount: data.data.followUser.followerCount,
                    followingCount: data.data.followUser.followingCount,
                    followsMe: data.data.followUser.followsMe,
                    followedByMe: data.data.followUser.followedByMe
                }
            })

        }).catch((reason) => {

            this.setState({
                followUserLoading: false
            })

            message.error(`Couldn't follow user. Check your Network Connection, Reason : ${reason}`)

        })
    }

}