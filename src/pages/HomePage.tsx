import * as React from 'react'
import { Container, Header } from 'semantic-ui-react'
import FeedBlotList from '../components/lists/FeedBlotList'
import { Redirect, RouteComponentProps } from 'react-router';
import { SessionContext } from '../data/contexts/SessionContext'

export default class HomePage extends React.Component<RouteComponentProps<{}>> {

    renderRedirect(): React.ReactNode {
        if (SessionContext.sessionToken === '') {
            return (
                <Redirect to="/login" push />
            )
        } else {
            return (<div />)
        }
    }

    render() {
        return (
            <div>
                {this.renderRedirect()}

                <div className="blotPage">
                    <Container text>
                        <FeedBlotList />
                    </Container>
                </div>
            </div>
        )
    }

}