import * as React from 'react'
import Center from 'react-center'
import TextField from '../components/TextField'
import HollowButton from '../components/HollowButton'
import { loginUsingAuth, getLoginToken } from '../data/api/APIAuth'
import { Input, Container, Button } from 'semantic-ui-react'
import { RouteComponentProps } from 'react-router';
import { message, Checkbox } from 'antd'

import '../styles/pages/LoginPage.css'
import { SessionContext } from '../data/contexts/SessionContext';

interface LoginPageState {

    loadingLogIn: boolean,
    rememberLogIn: boolean

}

export default class LoginPage extends React.Component<{}, LoginPageState> {

    state = {
        loadingLogIn: false,
        rememberLogIn: false
    }

    usernameField: TextField
    passwordField: TextField

    componentWillMount() {
        if (SessionContext.sessionToken !== "") {
            window.location.pathname = "/"
        }
    }

    render() {
        return (
            <div className="loginPage"> 
                <div className="loginPage-title">
                    Log In
                </div>
                <div className="loginPage-credentialEntryPanel">
                    <Container textAlign="center">
                        <TextField ref={(el) => this.usernameField = el} placeholder="Username" type={"text"} />
                    </Container>

                    <Container textAlign="center">
                        <TextField ref={(el) => this.passwordField = el} placeholder="Password" type={"password"} />
                    </Container>

                    <Center>
                        <Checkbox 
                            checked={this.state.rememberLogIn}
                            onChange={(e) => this.setState({ rememberLogIn: e.target.checked })}
                        >
                            Keep me signed in
                        </Checkbox>
                    </Center>
                </div>
                <Center>
                    <Button color="black" size="large" loading={this.state.loadingLogIn}
                        onClick={() => this.logIn()}>
                        log in
                    </Button>
                </Center>
                <div className="loginPage-registerContentArea">
                    <Center>
                        <div>New to blot ? <a className="loginPage-link" href={"/register"}>Create new Account</a></div>
                    </Center>   
                </div>
            </div>
        )
    }

    logIn() {

        this.setState({
            loadingLogIn: true
        })

        const username = this.usernameField.getText()
        const password = this.passwordField.getText()

        loginUsingAuth(username, password).then(() => {
            
            if (this.state.rememberLogIn) {
                getLoginToken().then(() => {
                    window.location.pathname = "/"
                }).catch((reason) => {
                    
                    message.error(`Remember Login failed, reason : ${reason}`)

                    console.log(reason)
                    
                    window.location.pathname = "/"

                    this.setState({
                        loadingLogIn: false
                    })
                })
            } else {
                window.location.pathname = "/"
            }

        }).catch(() => {
            
            message.error("There was an error logging in, check your username and password")

            this.setState({
                loadingLogIn: false
            })
        })
    }

}