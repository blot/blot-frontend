import * as React from 'react'
import { Input, Header, Container, Divider, Button, InputOnChangeData } from 'semantic-ui-react'
import ProfPicInput from '../components/ProfPicInput'
import Center from 'react-center'
import { createUser, loginUsingAuth } from '../data/api/APIAuth'
import { message } from 'antd'

interface RegisterAccountPageState {

    profPicLoading: boolean
    imgData?: string,
    validationSuccess: boolean,
    userNameVal: string,
    passwordVal: string,
    reEnterPasswordVal: string,
    displayNameVal: string,

    userNameError: boolean,
    passwordError: boolean,
    reEnterPasswordError: boolean,
    displayNameError: boolean,

    createButtonLoading: boolean
}

export default class RegisterAccountPage extends React.Component<{}, RegisterAccountPageState> {

    state = {
        profPicLoading: false,
        imgData: null,
        validationSuccess: false,
        userNameVal: '',
        passwordVal: '',
        reEnterPasswordVal: '',
        displayNameVal: '',
        userNameError: false,
        passwordError: false,
        reEnterPasswordError: false,
        displayNameError: false,
        createButtonLoading: false
    }

    constructor(props, ctx) {
        super(props, ctx)

        this.onUsernameChange = this.onUsernameChange.bind(this)
        this.onDisplayNameChange = this.onDisplayNameChange.bind(this)
        this.onPasswordChange = this.onPasswordChange.bind(this)
        this.onReEnterPasswordChange = this.onReEnterPasswordChange.bind(this)
    }

    render() {

        return (
            <div>
                <Container textAlign="center">
                    <Header as="h1" style={{ marginTop: "2em"}}>Create an Account</Header>
                    <Container text>
                        <div style={{ marginTop: "4em"}}>
                            <Center>
                                <ProfPicInput width={256} height={256} imageSet={(data) => this.onImageChange(data)}/>
                            </Center>
                        </div>
                    </Container>

                    <Container style={{ marginTop: "2em" }} text textAlign="center">
                        
                        <Input disabled={this.state.createButtonLoading} error={this.state.userNameError} onChange={this.onUsernameChange} placeholder="Username" style={{ width: "50%" }} /> <br /> 
                        <span style={{ color: "rgb(210, 210, 210)"}}>NOTE : Username cannot be changed afterwards</span>
                        <br /> <br />
                        <Input disabled={this.state.createButtonLoading} error={this.state.displayNameError} onChange={this.onDisplayNameChange} placeholder="Display Name" style={{ width: "50%" }}/> <br /> <br />
                        <Input disabled={this.state.createButtonLoading} error={this.state.passwordError} onChange={this.onPasswordChange} placeholder="Password" type="password" style={{ width: "50%" }}/> <br /> <br />
                        <Input disabled={this.state.createButtonLoading} error={this.state.reEnterPasswordError} onChange={this.onReEnterPasswordChange} placeholder="Re-enter Password" type="password" style={{ width: "50%" }}/> <br /> <br />
                        <div style={{ marginTop: "3em"}}>
                            <Button loading={this.state.createButtonLoading} onClick={() => this.createAccountClicked()} size="large" color="black">Create Account</Button>
                        </div>
                    </Container>
                </Container>
            </div>
        )

    }

    onImageChange(newImage: string) {
        this.setState({
            imgData: newImage
        })
    }

    createAccountClicked() {
        this.setState({
            createButtonLoading: true
        })

        createUser(this.state.userNameVal, this.state.passwordVal, this.state.displayNameVal, this.state.imgData).then(
            () => {
                message.success(`Account Creation Successfull!!! Welcome to Blot, ${this.state.userNameVal}`)

                loginUsingAuth(this.state.userNameVal, this.state.passwordVal).then(() => {

                    window.location.pathname = "/home"

                }).catch(() => {
                    window.location.pathname = "/login"
                })
            }
        ).catch(() => {
            message.error("Account Creation failed, the username maybe taken...")

            this.setState({
                createButtonLoading: false
            })
        })
    }

    onUsernameChange(e: React.SyntheticEvent<HTMLInputElement>, data: InputOnChangeData) {
        this.setState({
            userNameVal: data.value
        })

        this.validateUsernameText(data.value)
    }

    onPasswordChange(e: React.SyntheticEvent<HTMLInputElement>, data: InputOnChangeData) {
        this.setState({
            passwordVal: data.value
        })

        if (data.value.length < 5) this.setState({ passwordError: true })
        else this.setState({ passwordError: false })

        if (this.state.reEnterPasswordVal !== data.value) this.setState({ reEnterPasswordError: true })
        else this.setState({ reEnterPasswordError: false })             
    }

    onReEnterPasswordChange(e: React.SyntheticEvent<HTMLInputElement>, data: InputOnChangeData) {
        this.setState({
            reEnterPasswordVal: data.value
        })

        if (this.state.passwordVal !== data.value) this.setState({ reEnterPasswordError: true })
        else this.setState({ reEnterPasswordError: false })
    }

    onDisplayNameChange(e: React.SyntheticEvent<HTMLInputElement>, data: InputOnChangeData) {
        this.setState({
            displayNameVal: data.value
        })
        
        if (data.value == '') this.setState({ displayNameError: true })
        else this.setState({ displayNameError: false })
    }

    validateUsernameText(text: string) {
        if (text.length > 0) {
            if (!text.endsWith(" ") && !text.startsWith(" ")) {
                const regArray = text.match("[a-zA-Z0-9\\._\\-]{3,}")
                if (regArray) {
                    if (regArray.length != 1) this.setState({ userNameError: true })
                    else if (regArray[0] !== text) this.setState({ userNameError: true })
                    else this.setState({ userNameError: false })
                }
            } else this.setState({ userNameError: true })
        } else this.setState({ userNameError: true })
    }

    componentDidUpdate() {
        if (this.state.imgData && this.state.userNameVal.length > 0 && this.state.imgData.length > 0 && this.state.displayNameVal.length > 0 && this.state.passwordVal.length > 0 && this.state.reEnterPasswordVal.length > 0) {
            if (!(this.state.passwordError || this.state.userNameError || this.state.reEnterPasswordError || this.state.displayNameError)) {
                if (!this.state.validationSuccess) this.setState({ validationSuccess: true })
            } else if (this.state.validationSuccess) this.setState({ validationSuccess: false })
        }
    }
    
}