import * as React from 'react'

import '../styles/components/PageFooter.css'

export default class PageFooter extends React.Component {

    render() {
        return (
            <div className="footer">
                made with <span className="footer-heart">❤︎</span> by <a className="footer-link" href="/u/andrew">andrew bastin</a>
                &nbsp;with some help from <a className="footer-link" href="/u/calzseven07">calvin paiva</a>
            </div>
        )
    }

}