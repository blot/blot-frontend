import * as React from 'react'
import { Button, Container, Input, InputOnChangeData } from 'semantic-ui-react'
import { Modal } from 'antd'
import { Entity, ContentState, Modifier, ContentBlock, genKey } from 'draft-js'

interface EmbedModalProps {
    visible: boolean
    onClose: () => void
    onAddBlock: (srcURL: string) => void
}

interface EmbedModalState {
    srcURL: string
}

export default class EmbedModal extends React.Component<EmbedModalProps, EmbedModalState> {

    state = {
        srcURL: ""
    }

    constructor(props, ctx) {
        super(props, ctx)

        this.onTextChange = this.onTextChange.bind(this)
        this.onButtonClick = this.onButtonClick.bind(this)
    }

    render() {
        return (
            <Modal
                title="Add Embed"
                visible={this.props.visible}
                onOk={this.props.onClose}
                onCancel={this.props.onClose}
                footer={null}
                width={"25%"}
            >
                <Container>
                    <Input
                        fluid
                        focus={true}
                        onChange={this.onTextChange}
                        placeholder={"Enter embed code"}
                    />
                </Container>

                <Container style={{ marginTop: "2em" }} textAlign="right">
                    <Button disabled={this.state.srcURL == ''} color="black" onClick={this.onButtonClick}>Embed</Button>
                </Container>
            </Modal>
        )
    }

    onTextChange(el, data: InputOnChangeData) {
        this.setState({
            srcURL: data.value
        })
    }

    onButtonClick() {
        if (this.state.srcURL != '') {
            this.props.onAddBlock(this.state.srcURL)
            this.props.onClose()
        }
    }

}