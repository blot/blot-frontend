import * as React from 'react'
import { message, Modal } from 'antd'
import BlotComposeTextArea from '../editor/BlotComposeTextArea'
import { Container, Button, Icon } from 'semantic-ui-react'
import { apiPostQuery } from '../../data/api/APIGQLQuery'
import { Entity, AtomicBlockUtils, EditorState, ContentBlock, genKey } from 'draft-js';
import EmbedModal from './EmbedModal'
import * as Immutable from 'immutable'

interface ComposeBlotModalProps {
    visible: boolean
    onClose: () => void
}

interface ComposeBlotModalState {
    creatingPost: boolean,
    hasContent: boolean,
    boldOn: boolean,
    underlineOn: boolean,
    italicsOn: boolean,
    headerOn: boolean,
    isEmbedOpen: boolean
}

export default class ComposeBlotModal extends React.Component<ComposeBlotModalProps, ComposeBlotModalState> {

    composeField: BlotComposeTextArea

    state = {
        creatingPost: false,
        hasContent: false,
        boldOn: false,
        underlineOn: false,
        italicsOn: false,
        headerOn: false,
        isEmbedOpen: false
    }

    constructor(props, ctx) {
        super(props, ctx)

        this.handleComposeAreaChange = this.handleComposeAreaChange.bind(this)
        this.onAddEmbedBlock = this.onAddEmbedBlock.bind(this)
    }

    render() {
        return (
            <Modal
                title="New Blot"
                visible={this.props.visible}
                onOk={this.props.onClose}
                onCancel={this.props.onClose}
                footer={null}
                width={"50%"}
            >
                <div>
                    <BlotComposeTextArea ref={(el) => this.composeField = el} onChange={this.handleComposeAreaChange} />
                    {this.renderFooter()}
                    {this.renderModals()}
                </div>
            </Modal>
        )
    }

    handleComposeAreaChange(hasContent, editorState: EditorState) {
        
        var currentBlock = editorState.getCurrentContent().getBlockForKey(editorState.getSelection().getAnchorKey())

        Entity.create

        this.setState({
            hasContent: hasContent,
            boldOn: editorState.getCurrentInlineStyle().contains('BOLD'),
            italicsOn: editorState.getCurrentInlineStyle().contains('ITALIC'),
            underlineOn: editorState.getCurrentInlineStyle().contains('UNDERLINE'),
            headerOn: currentBlock.getType() === 'header-one'
        })

    }

    renderModals() {
        return (
            <div>
                <EmbedModal
                    onClose={() => this.setState({ isEmbedOpen: false })}
                    onAddBlock={this.onAddEmbedBlock}
                    visible={this.state.isEmbedOpen}
                />
            </div>
        )
    }

    onAddEmbedBlock(srcURL: string) {
        this.composeField.addEmbed(srcURL)
    }

    renderFooter() {

        return (
            <div>
                <Container style={{ marginTop: "2em" }}>
                    <Button.Group>
                        <Button color={ this.state.boldOn ? "black" : null } icon onClick={() => this.onStylePressed('BOLD')}><Icon name="bold" /> </Button>
                        <Button color={ this.state.italicsOn ? "black": null } icon onClick={() => this.onStylePressed('ITALIC')}><Icon name="italic" /></Button>
                        <Button color={ this.state.underlineOn ? "black" : null } icon onClick={() => this.onStylePressed('UNDERLINE')}><Icon name="underline" /></Button>
                    </Button.Group>
                    
                    <span style={{ marginLeft: "1em" }}>
                        <Button.Group>
                            <Button color={this.state.headerOn ? "black" : null} icon onClick={() => this.onStylePressed('header-one', false)}><Icon name="header" /></Button>
                        </Button.Group>
                    </span>

                    <span style={{ marginLeft: "1em" }}>
                        <Button.Group>
                            <Button icon onClick={() => this.setState({ isEmbedOpen: true })}><Icon name="cube" /></Button>
                        </Button.Group>
                    </span>
                    
                    <Button floated="right" color="black" disabled={!this.state.hasContent} loading={this.state.creatingPost} onClick={() => this.postBlot()}>Post</Button>
                </Container>
            </div>
        )

    }

    onStylePressed(styleName: string, isInline: boolean = true) {
        this.composeField.toggleStyle(styleName, isInline)
    }

    postBlot() {
        
        if (this.composeField.hasText()) {
        
            this.setState({
                creatingPost: true
            })
            
            apiPostQuery(
                `mutation { createPost(contentB64: \\"${window.btoa(this.composeField.getExportableData())}\\") { id } }`
            ).then(() => {
                this.setState({
                    creatingPost: false
                })

                this.composeField.clearText()
    
                this.props.onClose()
            }).catch((reason) => {
                this.setState({
                    creatingPost: false
                })
    
                message.error(`There was an error posting the blot, Reason : ${reason}`)
            })

        }
    }

}