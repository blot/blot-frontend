import * as React from 'react'
import { Label, Button, Icon, Image, Card } from 'semantic-ui-react'
import * as moment from 'moment'
import { Editor, convertFromRaw, EditorState, ContentBlock } from 'draft-js'
import CustomBlock from './editor/blocks/CustomBlock'
import { apiPostQuery } from '../data/api/APIGQLQuery'
import { PRODUCTION } from '../data/debug'

interface BlotProps {

    postedOn: string,
    postedBy: {
        username: string,
        displayName: string
    },
    contentB64: string,
    id: string
    likedByMe: boolean,
    likeCount: number
}

interface BlotState {
    editorState: EditorState,
    post: BlotProps
}

export default class Blot extends React.Component<BlotProps> {

    state = {
        editorState: EditorState.createWithContent(this.getValueFromContent()),
        post: this.props
    }

    render() {
        return (
            <Card fluid>
                <Card.Content>
                    <Image floated="left" size="mini" circular 
                    src={`${PRODUCTION ? "https://blot-backend.herokuapp.com" : "http://localhost:2299"}/content/prof_pic/mini/${this.props.postedBy.username}`} />
                    <Card.Header>
                        <a style={{ color: 'black' }} href={`/u/${this.props.postedBy.username}`}>{this.props.postedBy.displayName}</a>
                    </Card.Header>
                    <Card.Meta>
                        blotted this <strong>{moment(new Date(parseInt(this.props.postedOn))).fromNow()}</strong>
                    </Card.Meta>
                    <Card.Description>
                        <Editor
                            readOnly={true}
                            editorState={this.state.editorState}
                            onChange={() => {}}
                            blockRendererFn={(block: ContentBlock) => {
                                if (block.getType() === 'atomic') {

                                    return {
                                        component: CustomBlock,
                                        editable: false
                                    }

                                }
                            }}
                        />
                    </Card.Description>
                </Card.Content>

                <Card.Content extra>
                    <Button as='div' labelPosition='right'>
                        <Button color={this.state.post.likedByMe ? "red": null } icon 
                        onClick={() => {
                            if (!this.state.post.likedByMe) this.likePost() 
                            else this.unlikePost()
                            this.setState({ liked: !this.state.post.likedByMe})
                        } }>
                            <Icon color={this.state.post.likedByMe ? null : "red"} name='heart' />
                        </Button>
                        <Label as='a' basic color={ this.state.post.likedByMe ? "red" : null } pointing='left'>{this.state.post.likeCount}</Label>
                    </Button>
                </Card.Content>
            </Card>
        )
    }

    getValueFromContent() {
        return convertFromRaw(JSON.parse(window.atob(this.props.contentB64)))
    }

    likePost() {
        apiPostQuery(`mutation {likePost(id: \\"${this.props.id}\\") { likedByMe, likeCount } }`).then((res) => {

            this.setState({
                post: {
                    postedOn: this.state.post.postedOn,
                    postedBy: {
                        username: this.state.post.postedBy.username,
                        displayName: this.state.post.postedBy.displayName
                    },
                    contentB64: this.state.post.contentB64,
                    id: this.state.post.id,
                    likedByMe: res.data.likePost.likedByMe,
                    likeCount: res.data.likePost.likeCount
                }
            })

        })
    }

    unlikePost() {
        apiPostQuery(`mutation {unlikePost(id: \\"${this.props.id}\\") { likedByMe, likeCount } }`).then((res) => {

            this.setState({
                post: {
                    postedOn: this.state.post.postedOn,
                    postedBy: {
                        username: this.state.post.postedBy.username,
                        displayName: this.state.post.postedBy.displayName
                    },
                    contentB64: this.state.post.contentB64,
                    id: this.state.post.id,
                    likedByMe: res.data.unlikePost.likedByMe,
                    likeCount: res.data.unlikePost.likeCount
                }
            })

        })
    }

}