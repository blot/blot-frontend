import * as React from 'react'
import { message, Modal, Icon } from 'antd';
import { Button, Container } from 'semantic-ui-react'
import Cropper from 'react-cropper'
import Center from 'react-center'

import '../styles/components/ProfPicInput.css'
import 'cropperjs/dist/cropper.css'

interface ProfPicInputProps {
    width: number
    height: number,
    imageSet: (imageData: string) => void
}

interface ProfPicInputState {
    imageData: string,
    setImageData: string,
    cropModalOpen: boolean
}

export default class ProfPicInput extends React.Component<ProfPicInputProps, ProfPicInputState> {

    inputRef: HTMLInputElement

    state = {
        setImageData: null,
        imageData: null,
        cropModalOpen: false
    }

    constructor(props, ctx) {
        super(props, ctx)

        this.onInputChange = this.onInputChange.bind(this)
    }

    render() {
        if (this.state.setImageData == null) {
            return (
                <div style={{
                    width: `${this.props.width}px`,
                    height: `${this.props.height}px`
                }} className="profPicInput-container">
                    <div
                        style={{
                            position: 'absolute',
                            top: '15em',
                            paddingLeft: '2.1em'
                        }}
                    >
                        <Icon style={{ fontSize: 60 }} type="plus" />
                        <br /> <br />
                        <span style={{ marginLeft: '1em', marginRight: '1em' }}>
                            Upload Profile Image
                    </span>
                    </div>

                    <input ref={(el) => this.inputRef = el} className="profPicInput-input"
                        style={{ postion: 'absolute', opacity: 0, width: `${this.props.width}px`, height: `${this.props.height}px` }}
                        type="file" onChange={this.onInputChange} />

                    <ProfPicInputModal
                        visible={this.state.cropModalOpen}
                        src={this.state.imageData}
                        onCancel={() => this.onCancel()}
                        onDone={(data: string) => {
                            this.state.setImageData = data
                            this.setState({
                                cropModalOpen: false
                            })
                            this.props.imageSet(data)
                        }}
                    />
                </div>
            )
        } else {
            return (
                <div>
                    <div>
                        <img src={this.state.setImageData}
                            height={this.props.height}
                            width={this.props.width}
                            style={{ position: 'absolute', left: "50%" }}
                            className="profPicInput-container" />
                        <input ref={(el) => this.inputRef = el} className="profPicInput-input"
                            style={{ postion: 'absolute', opacity: 0, width: `${this.props.width}px`, height: `${this.props.height}px` }}
                            type="file" onChange={this.onInputChange} />
                    </div>

                    <ProfPicInputModal
                        visible={this.state.cropModalOpen}
                        src={this.state.imageData}
                        onCancel={() => this.onCancel()}
                        onDone={(data: string) => {
                            this.state.setImageData = data
                            this.setState({
                                cropModalOpen: false
                            })
                            this.props.imageSet(data)
                        }}
                    />
                </div>
            )
        }
    }
    
    onCancel() {
        this.setState({
            cropModalOpen: false
        })
        this.inputRef.value = ""   
    }

    onInputChange(evt: React.ChangeEvent<HTMLInputElement>) {
        const file = evt.target.files[0]

        if (file.type.startsWith("image/")) {

            const reader = new FileReader()
            reader.onloadend = () => {
                this.setState({
                    imageData: reader.result,
                    cropModalOpen: true
                })
            }
            reader.readAsDataURL(file)
            

        } else {
            evt.target.value = ''
            message.error("Please select a valid image file")
        }
    }

}



interface ProfPicInputModalProps {
    visible: boolean,
    src: string,
    onCancel: () => void,
    onDone: (croppedData: string) => void
}

interface ProfPicInputModalState {
}

class ProfPicInputModal extends React.Component<ProfPicInputModalProps> {

    private cropper: Cropper
    private imageView: HTMLImageElement

    constructor(props, ctx) {
        super(props, ctx)

    }

    render() {
        return (
            <Modal
                title="Crop Image"
                visible={this.props.visible}
                onOk={() => {}}
                onCancel={() => this.props.onCancel()}
                footer={null}
                width={"75%"}>
                
                <div style={{}}>
                    <Center>
                        <Cropper
                            ref={(el) => this.cropper = el}
                            src={this.props.src}
                            style={{ height: 400, width: '100%'}}
                            aspectRatio={1 / 1}
                            guides={false}
                            crop={() => {}} 
                            />
                    </Center>
                </div>

                <Container style={{ marginTop: "2em"}}textAlign="right">
                    <Button color="green"
                        onClick={() => {
                            this.props.onDone(this.cropper.getCroppedCanvas().toDataURL())
                        }}
                    >Done</Button>
                </Container>
            </Modal>
        )
    }

}