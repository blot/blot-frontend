import * as React from 'react'
import { Entity, Modifier, ContentState, Editor, EditorState, convertToRaw, 
        RichUtils, DraftHandleValue, ContentBlock, AtomicBlockUtils } from 'draft-js'
import CustomBlock from './blocks/CustomBlock'
import * as Immutable from 'immutable'

import '../../styles/components/BlotComposeTextArea.css'

interface BlotComposeTextAreaProps {

    onChange: (hasContent: boolean, editorState: EditorState) => void
}

interface BlotComposeTextAreaState {
    editorState: EditorState
}

export default class BlotComposeTextArea extends React.Component<BlotComposeTextAreaProps, BlotComposeTextAreaState> {

    textAreaRef: HTMLTextAreaElement

    state = {
        editorState: EditorState.createEmpty()
    }

    constructor(props, ctx) {
        super(props, ctx)

        this.onChange = this.onChange.bind(this)
    }

    render() {
        
        return (
            <Editor
                editorState={this.state.editorState} 
                onChange={this.onChange} 
                handleKeyCommand={this.handleKeyCommand}
                placeholder={"Blot down your thoughts..."}
                blockRendererFn={this.blockRendererFn}
            />
        )

    }

    private blockRendererFn(contentBlock: ContentBlock) {
        if (contentBlock.getType() === 'atomic') {

            return {
                component: CustomBlock,
                editable: false
            }

        }
    }

    updateCurrentBlock(embedEntityKey: string) {
        const editorState = this.state.editorState

        this.setState({
            editorState: AtomicBlockUtils.insertAtomicBlock(editorState, embedEntityKey, ' ')
        })
    }

    addEmbed(srcURL: string) {
        const contentState = this.state.editorState.getCurrentContent()
        const contentStateWithEntity = contentState.createEntity('embed', 'IMMUTABLE', { src: srcURL })
        const entityKey = contentStateWithEntity.getLastCreatedEntityKey()
        const editorState = this.state.editorState
        const newEditorState = EditorState.set(editorState, { currentContent: contentStateWithEntity })
        
        this.setState({
            editorState: AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey, ' ')
        })
    }

    private handleKeyCommand(command, editorState): DraftHandleValue {
        const newState = RichUtils.handleKeyCommand(editorState, command)
        if (newState) {
            this.onChange(newState)
            return "handled"
        } else return "not-handled"
    }

    private onChange(editorState) {
        this.setState({
            editorState: editorState
        })

        this.props.onChange(this.hasText(), editorState)
    }

    toggleStyle(styleName: string, isInline: boolean) {
        if (isInline) this.onChange(RichUtils.toggleInlineStyle(this.state.editorState, styleName))
        else this.onChange(RichUtils.toggleBlockType(this.state.editorState, styleName))
    }

    // Do not use for export
    getPlainText() {
        return this.state.editorState.getCurrentContent().getPlainText()
    }

    getExportableData() {
        return JSON.stringify(convertToRaw(this.state.editorState.getCurrentContent()))
    }

    hasText() {
        return this.state.editorState.getCurrentContent().hasText()
    }

    clearText() {
        this.setState({
            editorState: EditorState.createEmpty()
        })
    }

}