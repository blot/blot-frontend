import * as React from 'react'
import { ContentState, ContentBlock } from 'draft-js'
import EmbedBlock from './EmbedBlock'

interface CustomBlockProps {
    contentState: ContentState,
    block: ContentBlock
}

export default class CustomBlock extends React.Component<CustomBlockProps> {

    render() {
        const entity = this.props.contentState.getEntity(this.props.block.getEntityAt(0))
        
        if (entity.getType() === 'embed') {
            return (
                <EmbedBlock src={entity.getData().src} />
            )
        }

        return (<div>Unrecognized Custom Block</div>)
    }

}