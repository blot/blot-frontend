import * as React from 'react'

interface EmbedBlockProps {
    src: string
}

export default class EmbedBlock extends React.Component<EmbedBlockProps> {

    render() {
        return (
            <div dangerouslySetInnerHTML={this.getInnerHTML()} />
        )
    }

    getInnerHTML() {
        return {
            __html: this.props.src
        }
    }

}