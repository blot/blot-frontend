import * as React from 'react'

import '../styles/components/TextField.css'

interface TextFieldProps {

    placeholder: string,
    type: string

}

interface TextFieldState {

    text: string

}

export default class TextField extends React.Component<TextFieldProps, TextFieldState> {

    state = {
        text: ""
    }

    render() {

        return (
            <div className="textField-container">
                <input placeholder={this.props.placeholder} value={this.state.text} className="textField" type={this.props.type} onChange={(e) => this.setState({ text: e.target.value })} />
            </div>
        )

    }

    getText() {
        return this.state.text
    }

}