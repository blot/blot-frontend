import * as React from 'react'
import HollowButton from './HollowButton'
import { apiGetQuery } from '../data/api/APIGQLQuery'
import { closeSession } from '../data/api/APIAuth'
import { Menu, Divider, Button, Icon, Popup, Label, Container } from 'semantic-ui-react'
import { Input, Button as AntButton } from 'antd'
import ComposeBlotModal from './modals/ComposeBlotModal'
import Center from 'react-center'
import { PRODUCTION } from '../data/debug'

import '../styles/components/AppBar.css'
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';

interface AppBarState {
    loadingAccount: boolean,
    loggedIn: boolean,
    accountDisplayName: string,
    accountUsername: string,
    loggingOut: boolean,
    newBlotModalOpen: boolean,
    activePage: string
}

export default class AppBar extends React.Component<RouteComponentProps<{}>, AppBarState> {

    state = {
        loadingAccount: true,
        loggedIn: false,
        accountDisplayName: "",
        accountUsername: "",
        loggingOut: false,
        newBlotModalOpen: false,
        activePage: ''
    }


    private fetchAccountInfo() {
        apiGetQuery(`
            {
                me {
                    username, displayName
                }
            }
        `).then((data) => {
            if (data.data.me) {
                if (data.data.me.displayName && data.data.me.username) {
                    this.setState({
                        loadingAccount: false,
                        loggedIn: true,
                        accountDisplayName: data.data.me.displayName,
                        accountUsername: data.data.me.username
                    })
                    
                    if (this.props.history.location.pathname.startsWith("/home")) {
                        this.setState({ activePage: 'feed' })
                    } else if (this.state.loggedIn && this.props.history.location.pathname.startsWith(`/u/${this.state.accountUsername}`)) {
                        this.setState({ activePage: 'you' })
                    } else if (this.props.history.location.pathname.startsWith("/login")) {
                        this.setState({ activePage: 'login' })
                    }
                }
            } else {
                this.setState({
                    loadingAccount: false
                })
            }
        })
    }

    componentDidMount() {
        this.fetchAccountInfo()
        if (this.props.history.location.pathname.startsWith("/home")) {
            this.setState({ activePage: 'feed' })
        } else if (this.state.loggedIn && this.props.history.location.pathname.startsWith(`/u/${this.state.accountUsername}`)) {
            this.setState({ activePage: 'you' })
        } else if (this.props.history.location.pathname.startsWith("/login")) {
            this.setState({ activePage: 'login' })
        }

        this.props.history.listen((loc, action) => {
            if (this.props.history.location.pathname.startsWith("/home")) {
                this.setState({ activePage: 'feed' })
            } else if (this.state.loggedIn && this.props.history.location.pathname.startsWith(`/u/${this.state.accountUsername}`)) {
                this.setState({ activePage: 'you' })
            } else if (this.props.history.location.pathname.startsWith("/login")) {
                this.setState({ activePage: 'login' })
            }
        })
    }

    render() {
        return (
            <div className="appBar">
                <div className="appBar-title">
                    <img width={100} height={50} src="/branding/logo/logo-sidebar.png" />
                </div>
                <div className="appBar-routing">
                    {this.state.loggedIn ? 
                        <Link to="/home" style={{ textDecoration: 'none' }}>
                            <a className={this.getClassNameForID('feed')}>
                                my feed
                            </a>
                        </Link> 
                        : <div />} <br />
                    {this.state.loggedIn ? 
                        <Link to={`/u/${this.state.accountUsername}`} style={{ textDecoration: 'none' }}>
                            <a className={this.getClassNameForID('you')}>
                                me
                            </a>
                        </Link> 
                        : <div />} <br />
                </div>
                <div className="appBar-account">
                    {this.renderAccountArea()}
                </div>
            </div>
        )
    }

    private getClassNameForID(id: string): string {
        if (this.state.activePage === id) return "appBar-link appBar-link-current"
        else return "appBar-link"
    }

    private renderAccountArea(): React.ReactNode {
        if (this.state.loadingAccount) {
            return (
                <div>
                </div>
            )
        } else if (!this.state.loggedIn) {
            return (
                <div className="appBar-accountArea">
                    <Link to="/login" style={{ textDecoration: "none"}}>
                        <a className={this.getClassNameForID('login')}>log in</a>
                    </Link>
                </div>
            )
        } else {
            return (
                <div className="appBar-accountArea">
                    
                    <Container>
                        <a className="appBar-link" onClick={() => this.setState({ newBlotModalOpen: true })}>new blot</a> <br />
                    </Container>

                    <ComposeBlotModal 
                        visible={this.state.newBlotModalOpen}
                        onClose={() => this.setState({ newBlotModalOpen: false })}
                    />

                    <Popup
                        trigger={
                            <div className="appBar-popUpButton">
                                <img className="appBar-accountUserPic" src={`${PRODUCTION ? "https://blot-backend.herokuapp.com" : "http://localhost:2299"}/content/prof_pic/mini/${this.state.accountUsername}`} />
                                <span className="appBar-accountUserName">{this.state.accountDisplayName}</span>
                            </div>
                        }
                        on='click'
                        hideOnScroll
                        position='bottom right'
                        content={
                            <Container textAlign="right" fluid>
                                <div className="appBar-popUp-username">{this.state.accountDisplayName}</div>
                                <Label color="black">@{this.state.accountUsername}</Label>
                                
                                <Divider />

                                <Menu secondary vertical>
                                    <Menu.Item name="Edit Profile" active={false} />
                                    <Menu.Item name="Account Settings" active={false} 
                                    onClick={
                                        () => {
                                            console.log("Account Settings Pressed")
                                        }
                                    } />
                                </Menu>

                                <Divider />
                                
                                <Button loading={this.state.loggingOut} negative labelPosition="left" icon
                                    onClick={() => this.logOutUser()}
                                ><Icon name="sign out"/> Sign Out</Button>
                            </Container>
                        }
                    />
                </div>
            )
        }
    }

    private logOutUser() {
        if (this.state.loggedIn) {
            this.setState({
                loggingOut: true
            })

            closeSession().then(() => {
                window.location.reload(true)
            })
        }
    }

}