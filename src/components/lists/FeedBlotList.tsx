import * as React from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import { apiGetQuery } from '../../data/api/APIGQLQuery'
import { Container, Loader } from 'semantic-ui-react'
import Blot from '../Blot'
import Center from 'react-center'

interface BlotData {

    postedOn: string,
    postedBy: {
        username: string,
        displayName: string
    },
    contentB64: string,
    id: string,
    likedByMe: boolean,
    likeCount: number

}

interface FeedBlotListState {
    loadedBlots: React.ReactNode[],
    canLoadMore: boolean
    loadingData: boolean
}

export default class FeedBlotList extends React.Component<{}, FeedBlotListState> {

    state = {
        loadedBlots: [],
        canLoadMore: true,
        loadingData: false
    }

    constructor(props, ctx) {
        super(props, ctx)

        this.loadMoreBlots = this.loadMoreBlots.bind(this)
    }

    componentDidMount() {
        this.loadMoreBlots()
    }

    render() {

        return (
            <Container>
                <InfiniteScroll
                    style={{ overflow: "hidden" }}
                    dataLength={this.state.loadedBlots.length}
                    next={this.loadMoreBlots}
                    hasMore={this.state.canLoadMore}
                    refresh={() => this.setState({ loadedBlots: [] })}
                    loader={
                        <Loader active inline="centered" />
                    }
                    endMessage={
                        <Center>
                            <div style={{ color: "rgb(175, 175, 175)" }}>
                                {(this.state.loadedBlots.length > 0) ? "That's about it" : "There is nothing to show"}
                            </div>
                        </Center>
                    }
                >
                    {this.state.loadedBlots}
                </InfiniteScroll>
            </Container>
        )

    }

    loadMoreBlots() {
        this.setState({
            loadingData: true
        })

        apiGetQuery(
            `
            {
                getUserFeed(offset: ${this.state.loadedBlots.length}, limit: 10) {
                    id,
                    postedBy {
                        username,
                        displayName
                    },
                    contentB64,
                    postedOn,
                    likeCount,
                    likedByMe
                }
            }
            `
        ).then((data) => {
            console.log(data)
            if (data.data.getUserFeed) {
                this.setState({

                    loadedBlots: [...this.state.loadedBlots, ...data.data.getUserFeed.map((val: BlotData, index) => {
                        return (
                            <Center style={{ margin: "2em" }} key={val.id}>
                                <Blot key={val.id} contentB64={val.contentB64} id={val.id} postedBy={val.postedBy} postedOn={val.postedOn} likeCount={val.likeCount} likedByMe={val.likedByMe} />
                            </Center>
                        )
                    })],
                    canLoadMore: data.data.getUserFeed.length == 10,
                    loadingData: false,
                })
            }
        })
    }

}