import * as React from 'react'

import '../styles/components/HollowButton.css'

interface HollowButtonProps {

    text: String,
    onClick: () => void
    style?: React.CSSProperties
}

export default class HollowButton extends React.Component<HollowButtonProps> {

    render() {

        return (

            <div className="hollowButton-container">
                <button style={this.props.style} className="hollowButton" onClick={this.props.onClick}>{this.props.text}</button>
            </div>

        )

    }

}