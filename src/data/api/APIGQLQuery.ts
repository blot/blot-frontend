import axios, { AxiosRequestConfig } from 'axios'
import { SessionContext } from '../contexts/SessionContext';
import { PRODUCTION } from '../debug'

const API_GQLURL = `${PRODUCTION ? "https://blot-backend.herokuapp.com" : "http://localhost:2299"}/gql`

export function apiGetQuery(query: string): Promise<any> {

    return new Promise(

        (resolve, reject) => {
            axios.get(`${API_GQLURL}?query=${query.replace(/\s/g, "")}`, {
                headers: generateHeadersFromContext()
            }).then((res) => {
                resolve(res.data)
            }).catch((reason) => {
              reject(reason)  
            })

        }

    )

}

export function apiPostQuery(query: string): Promise<any> {
    return new Promise(

        (resolve, reject) => {

            axios.post(`${API_GQLURL}`, `{ "query": "${query}" }`, {
                headers: {
                    "Content-Type": "application/json",
                    ...generateHeadersFromContext()
                }
            }).then((res) => {
                resolve(res.data)
            }).catch((reason) => {
                reject(reason)
            })

        }

    )
}

function generateHeadersFromContext() {
    console.log(`Token : ${SessionContext.sessionToken}`)
    if (SessionContext.sessionToken !== '') {

        return {
            "Authorization": SessionContext.sessionToken
        }

    } else return {}
}