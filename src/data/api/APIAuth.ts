import axios from 'axios'
import { SessionContext } from '../contexts/SessionContext'
import { PersistentContext } from '../contexts/PersistentContext'
import { PRODUCTION } from '../debug'

const baseURL = `${PRODUCTION ? "https://blot-backend.herokuapp.com" : "http://localhost:2299"}`

export function loginUsingAuth(username: string, password: string): Promise<any> {

    return new Promise((resolve, reject) => {

        axios.get(`${baseURL}/auth/loginWithAuth`,
        {
            headers: {
                "Authorization": `Basic ${getHttpBasicAuthCredential(username, password)}`
            }
        }).then((data) => {

            SessionContext.sessionToken = (data.data as string)

            resolve(data.data)
        }).catch((reason) => {
            reject(reason)
        })

    })

}

export function createUser(username: string, password: string, displayName: string, profilePicB64: string): Promise<any> {

    const data = {
        username: username,
        password: password,
        displayName: displayName,
        profilePictureB64: profilePicB64
    }

    return new Promise((resolve, reject) => {

        axios.post(`${baseURL}/auth/createUser`, data).then((data) => {
            resolve(data)
        }).catch((reason) => reject(reason))

    })
}

export function getLoginToken(): Promise<any> {
    return new Promise(

        (resolve, reject) => {

            if (SessionContext.sessionToken !== "") {
                
                axios.get(`${baseURL}/auth/createLoginToken`,
                    {
                        headers: {
                            "Authorization": SessionContext.sessionToken
                        }
                    }
                ).then((data) => {

                    PersistentContext.loginToken = (data.data as string)

                }).catch((reason) => {
                    reject(reason)
                })

            } else {
                reject("Not logged in")
            }

        }

    )
}

export function closeSession(): Promise<any> {
    return new Promise (

        (resolve, reject) => {
            if (SessionContext.sessionToken != '') {
                axios.delete(`${baseURL}/auth/closeSession`, {
                    headers: {
                        "Authorization": SessionContext.sessionToken
                    }
                }).then((val) => {
                    
                    SessionContext.sessionToken = ""
                    
                    resolve(val)
                }).catch((reason) => reject(reason))
            } else reject("NO SESSION")

        }

    )
}

function getHttpBasicAuthCredential(username: string, password: string): string {
    const combined = `${username}:${password}`
    return window.btoa(combined)
}