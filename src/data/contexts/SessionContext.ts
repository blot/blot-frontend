var sessionToken: string = ""
var sessionTokenListeners: Array<(newVal: string) => void> = []

export const SessionContext = {
    
    get sessionToken(): string {
        return sessionToken
    },

    set sessionToken(x: string) {
        if (x) sessionToken = x
        else sessionToken = ""

        updateStore()

        sessionTokenListeners.forEach((val) => {
            val(x)
        })
    },


    onSessionTokenChange(func: (newVal: string) => void) {
        sessionTokenListeners.push(func)
    }

}

function createStoreSnapshot() {
    return {
        sessionToken: sessionToken
    }
}

function updateStore() {
    var snapshot = createStoreSnapshot()

    var snapString = JSON.stringify(snapshot)
    window.sessionStorage.setItem("context", snapString)
}

export function fetchSessionStore() {
    try {
        var storeString = window.sessionStorage.getItem("context")
        if (storeString !== null) {
            var data = JSON.parse(storeString)
            console.log(data)
            SessionContext.sessionToken = data.sessionToken
            console.log(SessionContext.sessionToken)
        }
    } catch (e) {
        window.sessionStorage.clear()
    }
}