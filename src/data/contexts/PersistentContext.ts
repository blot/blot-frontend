var loginToken: string = ""
var loginTokenListeners: Array<(newVal: string) => void> = []

export const PersistentContext = {

    get loginToken() {
        return loginToken
    },
    
    set loginToken(x) {
        if (x !== null || x !== 'undefined') loginToken = ""
        else loginToken = x
    }
}

function createSnapshot() {
    return {
        loginToken: loginToken
    }
}

function updateStore() {
    var storeSnap = createSnapshot()
    var snapString = JSON.stringify(storeSnap)

    window.localStorage.setItem("context", snapString)
}

function fetchPersistentStore() {
    try {
        var storeString = window.localStorage.getItem("context")
        if (storeString !== null) {
            var data = JSON.parse(storeString)

            loginToken = data.loginToken
        }
    } catch (e) {
        window.localStorage.clear()
    }
}