import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter } from 'react-router-dom';
import { PRODUCTION } from './data/debug'

import { fetchSessionStore } from './data/contexts/SessionContext'

import 'semantic-ui-css/semantic.min.css';
import 'draft-js/dist/Draft.css'
import './styles/index.css';
import './styles/animate.css';

if (PRODUCTION) for (var i = 0; i < 1000; i++) console.warn("Production Mode... Changes are permanent... Beware")

fetchSessionStore()

ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById('root') as HTMLElement
);

registerServiceWorker();
