import * as React from 'react'
import { Route, Redirect, Switch } from 'react-router-dom'
import LoginPage from './pages/LoginPage'
import HomePage from './pages/HomePage'
import UserPage from './pages/UserPage'
import RegisterAccountPage from './pages/RegisterAccountPage'
import PageFooter from './components/PageFooter'
import { SessionContext } from './data/contexts/SessionContext'
import AppBar from './components/AppBar'

import './styles/App.css'

class App extends React.Component {
  
  render() {
    return (
      <div className="app">
        <Route component={AppBar} />

        <div className="app-Content">
          <Switch>
            <Route path={"/u/:username"} component={UserPage} />
            
            <Route path={"/login"} render={ () => 
                  (SessionContext.sessionToken !== "") ? <Redirect to="/" /> : <LoginPage />              
            } />
            
            <Route path={"/home"} component={HomePage} />
            <Route path={"/register"} component={RegisterAccountPage} />

            <Route exact path={"/"}>
              {(SessionContext.sessionToken !== "") ? <Redirect to="/home" /> : <Redirect to="/login" />}
            </Route>

          </Switch>
        </div>

        <footer className="app-Footer">
          <PageFooter />
        </footer>
      </div>
    );
  }
}

export default App
